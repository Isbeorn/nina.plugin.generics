﻿#region "copyright"

/*
    Copyright © 2016 - 2021 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#endregion "copyright"

using Newtonsoft.Json;
using NINA.Core.Enum;
using NINA.Core.Model;
using NINA.Equipment.Interfaces.Mediator;
using NINA.Profile.Interfaces;
using NINA.Sequencer.Container;
using NINA.Sequencer.SequenceItem;
using NINA.Sequencer.Trigger;
using NINA.Sequencer.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace NINA.Plugin.Generics.Instructions {

    [ExportMetadata("Name", "Generic Trigger")]
    [ExportMetadata("Description", "")]
    [ExportMetadata("Icon", "")]
    [ExportMetadata("Category", "Generics")]
    [Export(typeof(ISequenceTrigger))]
    [JsonObject(MemberSerialization.OptIn)]
    public class GenericTrigger : SequenceTrigger, IValidatable {
        private IProfileService profileService;
        private ICameraMediator cameraMediator;
        private IFilterWheelMediator fwMediator;
        private IFocuserMediator focuserMediator;
        private IRotatorMediator rotatorMediator;
        private ITelescopeMediator telescopeMediator;
        private IGuiderMediator guiderMediator;
        private ISwitchMediator switchMediator;
        private IFlatDeviceMediator flatDeviceMediator;
        private IWeatherDataMediator weatherDataMediator;
        private IDomeMediator domeMediator;
        private ISafetyMonitorMediator safetyMonitorMediator;

        [ImportingConstructor]
        public GenericTrigger(IProfileService profileService,
                ICameraMediator cameraMediator,
                IFilterWheelMediator fwMediator,
                IFocuserMediator focuserMediator,
                IRotatorMediator rotatorMediator,
                ITelescopeMediator telescopeMediator,
                IGuiderMediator guiderMediator,
                ISwitchMediator switchMediator,
                IFlatDeviceMediator flatDeviceMediator,
                IWeatherDataMediator weatherDataMediator,
                IDomeMediator domeMediator,
                ISafetyMonitorMediator safetyMonitorMediator) {
            this.profileService = profileService;
            this.cameraMediator = cameraMediator;
            this.fwMediator = fwMediator;
            this.focuserMediator = focuserMediator;
            this.rotatorMediator = rotatorMediator;
            this.telescopeMediator = telescopeMediator;
            this.guiderMediator = guiderMediator;
            this.switchMediator = switchMediator;
            this.flatDeviceMediator = flatDeviceMediator;
            this.weatherDataMediator = weatherDataMediator;
            this.domeMediator = domeMediator;
            this.safetyMonitorMediator = safetyMonitorMediator;

            Properties = new List<PropertyInfo>();
            Comparators = new List<ComparisonOperatorEnum>();
            Devices = new List<string>() {
                "Camera",
                "Filter Wheel",
                "Focuser",
                "Rotator",
                "Telescope",
                "Guider",
                "Switch",
                "Flat Panel",
                "Weather",
                "Dome",
                "Safety Monitor"
            };
            SelectedDevice = "Camera";
        }

        private GenericTrigger(GenericTrigger copyMe) : this(copyMe.profileService,
                                                                 copyMe.cameraMediator,
                                                                 copyMe.fwMediator,
                                                                 copyMe.focuserMediator,
                                                                 copyMe.rotatorMediator,
                                                                 copyMe.telescopeMediator,
                                                                 copyMe.guiderMediator,
                                                                 copyMe.switchMediator,
                                                                 copyMe.flatDeviceMediator,
                                                                 copyMe.weatherDataMediator,
                                                                 copyMe.domeMediator,
                                                                 copyMe.safetyMonitorMediator) {
            CopyMetaData(copyMe);
            this.TriggerRunner = (SequentialContainer)copyMe.TriggerRunner.Clone();
        }

        public override object Clone() {
            return new GenericTrigger(this) {
                SelectedDevice = this.SelectedDevice,
                SelectedProperty = this.SelectedProperty,
                SelectedComparator = this.SelectedComparator,
                Value = this.Value
            };
        }

        private object GetMediator() {
            switch (SelectedDevice) {
                case "Camera": return cameraMediator;
                case "Filter Wheel": return fwMediator;
                case "Focuser": return focuserMediator;
                case "Rotator": return rotatorMediator;
                case "Telescope": return telescopeMediator;
                case "Guider": return guiderMediator;
                case "Switch": return switchMediator;
                case "Flat Panel": return flatDeviceMediator;
                case "Weather": return weatherDataMediator;
                case "Dome": return domeMediator;
                case "Safety Monitor": return safetyMonitorMediator;
                default: return null;
            }
        }

        public override bool AllowMultiplePerSet => true;

        public List<string> Devices { get; }
        public List<PropertyInfo> Properties { get; private set; }
        public List<ComparisonOperatorEnum> Comparators { get; private set; }

        private string selectedDevice;

        [JsonProperty]
        public string SelectedDevice {
            get => selectedDevice;
            set {
                selectedDevice = value;
                DetermineProperties();
                RaisePropertyChanged();
            }
        }

        private void DetermineProperties() {
            var mediator = GetMediator();
            var type = mediator.GetType();
            var getInfo = type.GetMethod("GetInfo");
            var deviceInfo = getInfo.Invoke(mediator, null);

            Properties = new List<PropertyInfo>(deviceInfo.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive).OrderBy(x => x.Name));
            SelectedProperty = Properties.First().Name;
            RaisePropertyChanged(nameof(Properties));
        }

        private PropertyInfo selectedProperty;

        [JsonProperty]
        public string SelectedProperty {
            get => selectedProperty.Name;
            set {
                selectedProperty =  Properties.First(x => x.Name == value);
                DetermineComparator();
                RaisePropertyChanged();
            }
        }

        private void DetermineComparator() {
            if (selectedProperty.PropertyType == typeof(bool)) {
                Comparators = new List<ComparisonOperatorEnum>() { ComparisonOperatorEnum.EQUALS, ComparisonOperatorEnum.NOT_EQUAL };
            } else {
                Comparators = new List<ComparisonOperatorEnum>(Enum.GetValues(typeof(ComparisonOperatorEnum)).OfType<ComparisonOperatorEnum>());
            }
            SelectedComparator = Comparators.First();
            Value = 0;
            RaisePropertyChanged(nameof(Comparators));
        }

        private ComparisonOperatorEnum slectedComparator;

        [JsonProperty]
        public ComparisonOperatorEnum SelectedComparator {
            get => slectedComparator;
            set {
                slectedComparator = value;
                RaisePropertyChanged();
            }
        }

        private double value;

        [JsonProperty]
        public double Value {
            get => value;
            set {
                this.value = value;
                Validate();
                RaisePropertyChanged();
            }
        }

        private IList<string> issues = new List<string>();
        public IList<string> Issues { get => issues; set { issues = value; RaisePropertyChanged(); } }

        public bool Validate() {
            var i = new List<string>();

            //var profileId = GetProfileId();
            //if (profileId == "No_Device" || profileId == "No_Guider") {
            //    i.Add($"There is no device id stored in the profile for the {SelectedDevice}. Make sure to manually connect a {SelectedDevice} once, so that a device id for a {SelectedDevice} is stored in the profile.");
            //}

            Issues = i;
            return i.Count == 0;
        }

        public override async Task Execute(ISequenceContainer context, IProgress<ApplicationStatus> progress, CancellationToken token) {
            await TriggerRunner.Run(progress, token);
        }

        public override bool ShouldTrigger(ISequenceItem previousItem, ISequenceItem nextItem) {
            var mediator = GetMediator();
            var type = mediator.GetType();            
            var getInfo = type.GetMethod("GetInfo");
            var deviceInfo = getInfo.Invoke(mediator, null);

            double currentValue = Convert.ToDouble(selectedProperty.GetValue(deviceInfo));

            switch (SelectedComparator) {
                case ComparisonOperatorEnum.LESS_THAN:
                    if (currentValue < Value) { return true; }
                    break;

                case ComparisonOperatorEnum.LESS_THAN_OR_EQUAL:
                    if (currentValue <= Value) { return true; }
                    break;

                case ComparisonOperatorEnum.GREATER_THAN:
                    if (currentValue > Value) { return true; }
                    break;

                case ComparisonOperatorEnum.GREATER_THAN_OR_EQUAL:
                    if (currentValue >= Value) { return true; }
                    break;

                case ComparisonOperatorEnum.EQUALS:
                    if (currentValue == Value) { return true; }
                    break;

                case ComparisonOperatorEnum.NOT_EQUAL:
                    if (currentValue != Value) { return true; }
                    break;
            }

            return false;
        }

        public override string ToString() {
            return $"Category: {Category}, Item: {nameof(GenericTrigger)}, SelectedDevice: {SelectedDevice}, SelectedProperty: {SelectedProperty}, Comparator: {SelectedComparator}, Value: {Value}";
        }
    }
}